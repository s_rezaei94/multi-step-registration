<?php

namespace App\AppClass;

use App\AppInterface\AddressDTOInterface;
use App\AppDTO\AddressDTO;
use ArrayIterator;
use Iterator;


class AddressCollection {
    
    /** @var array $address */
    private  $address;
    public function __construct(array $address){
        $this->address = $address;
    }
    public function get(int $index): AddressDTOInterface{
        return new AddressDTO($this->address[$index]);
    }
    public function getAll(): Iterator{
        return new ArrayIterator($this->address);
    }
    public function getCount(): int{
        return count($this->address);
    }

}