<?php

namespace App\AppDTO;

use App\AppInterface\UserDTOInterface;
use App\AppClass\AddressCollection;
use App\AppDTO\PaymentInfoDTO;

class UserDTO  implements UserDTOInterface{
    
    private $firstName;
    private $lastName;
    private $tel;
    private $address;
    private $paymentInfo;
    public function __construct($data){

        $this->map($data);
    }
    //TODO validate
    public function map($data){
        
        foreach($data as $key => $value){
            if (property_exists($this, $key)) {
                
                switch($key){
                    case "paymentInfo":
                        $this->$key = new PaymentInfoDTO($value);    
                        break;
                    case "address":
                        $this->$key = new AddressCollection($value);
                        break;
                    default:
                        $this->$key = $value;
                        break;
                }
            }
        }
        

    }
    public function getFirstName(){
        return $this->firstName;
    }
    public function getLastName(){
        return $this->lastName;
    }
    public function getTel(){
        return $this->tel;
    }
    public function getAddress(){
        return $this->address;
    }
    public function getPaymentInfo(){
        return $this->paymentInfo;
    }
    
}