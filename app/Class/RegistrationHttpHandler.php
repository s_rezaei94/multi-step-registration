<?php

namespace App\AppClass;

use  App\AppClass\HttpHandler;
use App\Util\Logger;

class RegistrationHttpHandler extends HttpHandler{

    use Logger;
    private $url;
    private $userId;
    private $IBAN;
    private $accountOwner;
    public function __construct(string $url ,  int $attempted , int $userId , string $IBAN , string $accountOwner){
        
        $this->url = $url;
        $this->attempted = $attempted;
        $this->userId = $userId;
        $this->IBAN = $IBAN;
        $this->accountOwner = $accountOwner;

    }
    public function getData(){
        try{
            
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>"{
                    'customerId': {$this->userId},
                    'iban': '{$this->IBAN}',
                    'owner': '{$this->accountOwner}'
                }",
                CURLOPT_HTTPHEADER => array(
                    //we need it
                    'Authorization: ' . AUTH_TOKEN,
                    'Content-Type: application/json'
                ),
            ));
    
            $response = curl_exec($curl);
            $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
            }
            curl_close($curl);
            if($response < 200 and $response >= 300)
                throw new Exception("Some error in RegisterationHttpHandler -> " . json_encode($response));
            if (isset($error_msg)) {
                // TODO - Handle cURL error accordingly
                var_dump($error_msg);die;
            }
            
                
            $response = json_decode($response , true);

        }catch(Exception $e) {

            $this->log($e->getMessage());
            return false;
        }
        return $response;

    }
    public function checkData($data){
        //do some check if data is valid
        return (!$data OR empty($data))? false : true;

    }
    public function serviceUnAvailable(){
        // do some stuff like alerting 
        //or throw exception
    }
    
}