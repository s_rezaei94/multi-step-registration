<?php

namespace App\Model;
use App\Util\Logger;

class UserModel{
    use Logger;
    private $id;
    private $first_name;
    private $last_name;
    private $tel;
    private $tableName = "user";

    public function __construct($id , $first_name , $last_name  , $tel){
        $this->setFirstName($first_name);
        $this->setLastName($last_name);
        $this->setTel($tel);
        $this->setId($id);
    }

    public function save($dbAdapter){
        $result = $dbAdapter->insert(
            $this->tableName,
            $this
        );
        if($result)
            return  $dbAdapter->insertId();
        $result = $dbAdapter->get($this->tableName, ["tel" => $this->tel] );
        
        if($result and count($result) > 0){
            $this->log("Duplicated entry for user with the same Tel");
            $this->outputLog("Duplicated entry for user with the same Tel");
        }
            
        
        $this->log("cant insert in UserModel");
        $this->outputLog();
        
    }
    public function remove($dbAdapter){
        $result = $dbAdapter->delete(
            $this->tableName,
            $this
        );
        if($result)
            return true;
        
        $this->log("cant Remove in UserModel");
        $this->outputLog();

    }
    public function setFirstName($first_name){
        //TODO do some validation
        $this->first_name = $first_name;
    }
    public function setLastName($last_name){
        //TODO do some validation
        $this->last_name = $last_name;
    }
    public function setTel($tel){
        //TODO do some validation
        $this->tel = $tel;
    }
    public function setId($id){
        //TODO do some validation
        $this->id = $id;
    }

    public function getProperties(){
        $properties = get_object_vars($this);
        //ommit the not usable field
        unset($properties['tableName']);
        return $properties;
    }
    
}