<?php

namespace App\AppClass;

use App\AppInterface\DatabaseAdapterInterface;
use App\Util\Logger;
use PDO;

class MysqlAdapter implements DatabaseAdapterInterface{
    use Logger;
    public $conn;
    private $host;
    private $dbName;
    private $userName;
    private $password;
    private static $instance;
    protected function __construct(){}
    protected function __clone() { }
    public static function getInstance(string $host , string $dbName , string $userName , string $password): MysqlAdapter{

        if(!isset(self::$instance)){

            self::$instance = new static();
            self::$instance->host = $host;
            self::$instance->dbName = $dbName;
            self::$instance->userName = $userName;
            self::$instance->password = $password;
            self::$instance->connect();
        }
        return self::$instance;

    }
    private function connect(){
        
        try {
            $this->conn = new PDO("mysql:host={$this->host};dbname={$this->dbName}", $this->userName, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
          } catch(\PDOException $e) {
              $this->log($e->getMessage());
              $this->outputLog();
  
            
          }

    }
    public function insert(string $table  ,  $params){
        $result = false;
        try{
            

            if(!is_array($params))
                $params = $params->getProperties();
                
            $params = $this->removeEmptyData($params);
            $query = $this->insertQueryBuilder($table , $params);
            
            $stm = $this->conn->prepare($query);
            $result = $stm->execute(array_values($params));
        } catch(\PDOException $e) {
            
            $this->log($e->getMessage());
            
            
        }
        return $result;
    }
    public function insertId(){
        return $this->conn->lastInsertId();
    }
    public function get(string $table , array $condition){

        $result = false;
        try{
            $query = $this->selectQueryBuilder($table , $condition);
            $stm = $this->conn->prepare($query);
            $result = $stm->execute(array_values($condition));
            $result = $stm->fetch(PDO::FETCH_ASSOC);
        } catch(\PDOException $e) {
            
            $this->log($e->getMessage());
            
        }
        return $result;

    }
    public function delete(string $table , $condition){
        $result = false;
        try{

            if(!is_array($condition))
                $condition = $condition->getProperties();
            $condition = $this->removeEmptyData($condition);

            $query = $this->deletetQueryBuilder($table , $condition);
            $stm = $this->conn->prepare($query);
            $result = $stm->execute(array_values($condition));
        } catch(\PDOException $e) {
            
            $this->log($e->getMessage());
            
        }
        return $result;
    }
    public function update(string $table , $updateFields , $condition){
        $result = false;
        try{

            if(!is_array($condition))
                $condition = $condition->getProperties();
            $condition = $this->removeEmptyData($condition);
            $updateFields = $this->removeEmptyData($updateFields);

            $query = $this->updateQueryBuilder($table , $updateFields ,  $condition);
            
            $temp = array_merge(array_values($updateFields) , array_values($condition));
            $stm = $this->conn->prepare($query);
            $result = $stm->execute($temp);
        } catch(\PDOException $e) {
            
            $this->log($e->getMessage());
            
        }
        return $result;
    }
    private function deletetQueryBuilder(string $table , array $condition){
        $query  = "DELETE  FROM `${table}` ";
        $query .= $this->createCondition($condition);
        return $query;
    }
    private function selectQueryBuilder(string $table , array $condition){
        $query  = "SELECT * FROM `${table}` ";

        $query .= $this->createCondition($condition);
        return $query;
    }
    private function createCondition(array $condition){
        if(empty($condition))
            return "";
        $query = " WHERE ";
        foreach($condition AS $key => $value){
            $query .= "`{$key}` = ? AND";
        }
        $query = rtrim($query , "AND");
        return $query;
    }
    private function insertQueryBuilder(string $table , array $params){

        $query  = "INSERT INTO `" . $table . "` ( `" . implode('`,`' , array_keys($params)) . '` )' . " VALUES ( " .
            rtrim(str_repeat("?," , count($params)),",") . " )" ;
        return $query;

    }
    private function updateQueryBuilder(string $table , array $updateFields , array $params){
            
        $query  = "UPDATE  `" . $table . "` SET " ;

        foreach ($updateFields AS $key => $value){
            $query .= "" . $key . " =  ?"  . " , ";
        }
        $query = rtrim($query , ', ');
        $query .= $this->createCondition($params);
        return $query;

    }
    private function removeEmptyData(array $params){
        //remove empty value from array if exist
        $params = array_filter($params , function ( $value , $key){
            if(
                    $value === 0 OR
                    $value === false OR
                    $value === '0' OR
                    !empty($value) 
            ) return true;

        } , ARRAY_FILTER_USE_BOTH  );
        return $params;
    }
}