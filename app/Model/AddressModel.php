<?php

namespace App\Model;
use App\Util\Logger;

class AddressModel{
    use Logger;
    private $id;
    private $street;
    private $house_number;
    private $zip_code;
    private $city;
    private $user_id;
    private $tableName = "address";

    public function __construct($user_id , $street , $house_number  , $zip_code , $city){

        $this->setUserId($user_id);
        $this->setStreet($street);
        $this->setHouseNumber($house_number);
        $this->setZipCode($zip_code);
        $this->setCity($city);

    }

    public function save($dbAdapter){
        $result = $dbAdapter->insert(
            $this->tableName,
            $this
        );
        if($result)
            return  $dbAdapter->insertId();
        
        $this->log("cant insert in AddressModel");
        $this->outputLog();
        
    }
    public function remove($dbAdapter){
        $result = $dbAdapter->delete(
            $this->tableName,
            $this
        );
        if($result)
            return true;
        
        $this->log("cant Remove in AddressModel");
        $this->outputLog();

    }
    public function setUserId($user_id){
        //TODO do some validation
        $this->user_id = $user_id;
    }
    public function setStreet($street){
        //TODO do some validation
        $this->street = $street;
    }
    public function setHouseNumber($house_number){
        //TODO do some validation
        $this->house_number = $house_number;
    }
    public function setZipCode($zip_code){
        //TODO do some validation
        $this->zip_code = $zip_code;
    }
    public function setCity($city){
        //TODO do some validation
        $this->city = $city;
    }
    public function getProperties(){
        $properties = get_object_vars($this);
        //ommit the not usable field
        unset($properties['tableName']);
        return $properties;
    }
    
}