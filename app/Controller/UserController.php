<?php

namespace  App\Controller;


use App\AppService\UserRegistrationService;
use App\AppService\PaymentRegistrationService;
use App\AppClass\MysqlAdapter;
use App\Util\Logger;
use App\AppDTO\UserDTO;

class UserController{
    use Logger;
    public function __construct(){
        
    }
    public function test(){
        echo json_encode(["paymentDataId" => "c68644153714ca78e4102c7f54747a5a3c1c06be332bd4c2b26e
                            7b2a41ed228d86975c9997b96b8bb0da030d34a2be95"]);die;
    }
    public function create(){

        
        try{
            $data = $_POST;
            if(empty($data)){
                $json = file_get_contents('php://input');
                $data = json_decode($json, true);
                if ($data === null
                    && json_last_error() !== JSON_ERROR_NONE) {
                        throw new Exception("Error in parsing input data");
                }

            }
            

            $db = MysqlAdapter::getInstance(
                getenv("MYSQL_HOST"),
                "wunder",
                getenv("MYSQL_USER"),
                getenv("MYSQL_PASSWORD")
            );

            //Validate data if necessary and put it in object way
            $user = new UserDTO($data);
            
            $userId  = 36;
            //call a service to help registering user
            $userRS = new UserRegistrationService($db);
            $userId = $userRS->register(
                $user        
            );

            $data = (new PaymentRegistrationService($db))->registerInfo(
                $userId,
                $user->getPaymentInfo()->getIBAN(),
                $user->getPaymentInfo()->getAccountOwner()
            );
            
            if($data and isset($data['paymentDataId'])){
                //save the code to database
                $userRS->updatePayment($userId , $data['paymentDataId']);
            }else{
                //rollback
                $userRS->rollBack($userId);
                throw new \Exception("There was an error getting paymentDataId!! " . json_encode($data));
            }

        

        }catch(\Exception $e){
            $this->log($e->getMessage());
            $this->outputLog($e->getMessage());
            die;
        }

        echo json_encode(["result" => true , "message" => "Data imported successfully" , "paymentDataId" => $data['paymentDataId']]);



    }    


}