<?php

namespace App\AppService;

use App\AppInterface\DatabaseAdapterInterface;
use  App\Model\UserModel;
use  App\Model\AddressModel;
use  App\Model\PaymentInfoModel;
use App\AppDTO\UserDTO;

class UserRegistrationService{
    
    private $adapter;
    public function __construct(DatabaseAdapterInterface $adapter){
        
        $this->adapter = $adapter;
    }
    public function register(UserDTO $user){

        
        $userId = (new UserModel(
            null,
            $user->getFirstName(),
            $user->getLastName(),
            $user->getTel()

        ))->save($this->adapter);
        $addressCount = $user->getAddress()->getCount();
        for($i = 0 ; $i < $addressCount ; $i++){
            $address = $user->getAddress()->get($i);
            
            (new AddressModel(

                $userId,
                $address->getStreet(),
                $address->getHouseNumber(),
                $address->getZipCode(),
                $address->getCity()
                
            ))->save($this->adapter);

        }

        (new PaymentInfoModel(

            $userId,
            $user->getPaymentInfo()->getAccountOwner(),
            $user->getPaymentInfo()->getIBAN()
            
        ))->save($this->adapter);

        return $userId;

    }
    public function rollBack(int $userId){

        (new UserModel(
            $userId,
            null,
            null,
            null
        ))->remove($this->adapter);
        (new AddressModel(

            $userId,
            null,
            null,
            null,
            null
            
        ))->remove($this->adapter);
        (new PaymentInfoModel(

            $userId,
            null,
            null
            
        ))->remove($this->adapter);

    }
    public function updatePayment(int $userId , $paymentDataId){

        (new PaymentInfoModel(

            $userId,
            null,
            null,
            $paymentDataId
            
        ))->update($this->adapter);

    }
}
