## Multi Step Registration



## Technologies:

-  MySQL 8
-  PHP7.4
-  Docker
-  Nginx
-  Composer
## How to run:

- Make sure you have an internet connection to download dependencies.
- Make sure you have docker and docker-compose up and running on your computer; if not take a look at the below links:
    - docker desktop [installation](https://www.docker.com/get-started)
    - docker [installation](https://docs.docker.com/engine/install/)
    - docker-compose [installation](https://docs.docker.com/compose/install/)
- Download the git repository
- Open your terminal in the project directory
- Run `docker-compose up -d --build` command to run the project in the background.
    - Application will run on port `8081` , make sure the port is free or is not blocked.
- Be patient it takes a minute to download the dependencies.
  - For monitoring the events run `docker-compose logs -f` at the root of the project, the installer will print `*************** Done ***************` when install completed.
- Application is now running `http://localhost:8081` address

Note: if there was an error, or it takes too long to install, please remove the `mysql` folder which is located in the `docker` folder, and run the installation again.

## How to use it:


Use  `http://localhost:8081/public` to see the form.


## Questions:

1-We can dont use rollback when the API had an error instead we can send back a JWT token to users and make the request for retry to connect to the API with that Token.

2-we can add the following features to make sure the app is stable.

- Add some validation in DTO and validation in Models

- Add UnitTests

- Refactor the code

- Monitoring the external endpoint

- Make sure the API generating unique paymentDataId


## Note:

As I was testing the Endpoint it figures out that the endpoint require `Authentication Token` So as I did not have any Token I put an endpoint in the application which return `paymentDataId` to test the application.
You can add `AUTH_TOKEN` in the `config/constant.php` and uncomment the `PAYMENT_ENDPOINT` which is based on the external endpoint.


