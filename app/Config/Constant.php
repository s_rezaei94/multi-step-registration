<?php

define("ROOT_PATH" , dirname(__DIR__  , 2) . "/");
define("APP_PATH" , dirname(__DIR__ ) . "/");
define("LOG_PATH" , dirname(__DIR__ ) . "/assets/logs/");

// define("PAYMENT_ENDPOINT" , "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-b%20ackend-dev-save-payment-data");
define("PAYMENT_ENDPOINT" , "http://nginx-service/test");

//if it need Bearer make sure to put it inside
define("AUTH_TOKEN" , "");
