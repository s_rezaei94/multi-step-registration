<?php

namespace App\AppClass;


use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use App\Util\Logger;

class Router {
    use Logger;
    private $routes;
    private $currentController;
    private $currentMethod;
    private $params;
    public function __construct(RouteCollection $routes){
        $this->routes = $routes;
    }
    public function match(){

        try{

            // Init RequestContext object
            $context = new RequestContext();
            $context->fromRequest(Request::createFromGlobals());

            // Init UrlMatcher object
            $matcher = new UrlMatcher($this->routes, $context);

            // Find the current route
            $parameters = $matcher->match($context->getPathInfo());
            return $parameters;
        
        }
        catch (ResourceNotFoundException $e)
        {
            
            $this->log($e->getMessage());
            $this->outputLog();
            
        }

    }
    public  function runApp(array $params){
        
        
        if(isset($params['controller']) and file_exists("./app/Controller/{$params['controller']}.php")){
            $this->currentController = "App\Controller\\" . $params['controller'];
            $this->currentController = new  $this->currentController;
            $params["method"] = (isset($params["method"]))? $params["method"] : "index";
            unset($params["controller"]);
            unset($params["_route"]);
            
            if(method_exists($this->currentController, $params["method"])){
                // Set current method if it exsists
                $this->currentMethod = $params["method"];
                // Unset 1 index
                unset($params["method"]);
            }
            
            // Get params - Any values left over in url are params
            $this->params = $params ? array_values($params) : [];
            
            //Call the function within the controller
            call_user_func_array([$this->currentController, $this->currentMethod], $this->params);
            return;
        }
        $this->log("error routing");
            $this->outputLog();

    }
    public function route(){
        $params = $this->match();
        $this->runApp($params);
    }

}