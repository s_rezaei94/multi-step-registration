<?php

namespace App\AppService;

use App\AppInterface\DatabaseAdapterInterface;
use  App\Model\PaymentInfoModel;
use App\AppClass\RegistrationHttpHandler;

class PaymentRegistrationService{
    
    private $adapter;
    public function __construct(DatabaseAdapterInterface $adapter){
        
        $this->adapter = $adapter;

    }
    public function registerInfo(int $userId , string $IBAN , string $accountOwner){


        $data = (new RegistrationHttpHandler(
            PAYMENT_ENDPOINT,
            2,
            $userId,
            $IBAN,
            $accountOwner
        ))->process();

        return $data;

    }
}
