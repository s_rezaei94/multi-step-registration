$(document).ready(function(){

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;


    if(localStorage.getItem('step')){
        step = parseInt(localStorage.getItem('step'));
        if(step < 1 || step > 4)
            step = 1;
        next_fs = $(`form[step=${step}]`);
        if(step >1 ){
            current_fs = $(`form[step=1]`);
            stepManager(current_fs , next_fs);
        }
        mapData(next_fs);
        //show the next form
        next_fs.show();
    }
    
    $(".next").click(function(){


        if(this.name == "make_payment"){
            sendData();
        }
        
        current_fs = $(this).parents('form');
        step = current_fs.attr('step');
        nextStep = parseInt(step)+1;
        next_fs = $(this).parents('form').next();
        
        $(`form[step=${step}]`).validate();
        if(!$(`form[step=${step}]`).valid())
            return;
        //save data to local storage
        saveData(current_fs);
        //show the next form
        next_fs.show();
        localStorage.setItem("step" , nextStep);
        //hide the current form with style
        stepManager(current_fs , next_fs );
    });

    $(".previous").click(function(){

    current_fs = $(this).parents('form');
    step = current_fs.attr('step');
    prevStep = parseInt(step)-1;
    previous_fs = $(this).parents('form').prev();

    //Remove class active
    $("#progressbar li").eq($("form").index(current_fs)).removeClass("active");

        //show the previous form
        previous_fs.show();
        localStorage.setItem("step" , prevStep);
        //hide the current form with style
        stepManager(current_fs , previous_fs);
        mapData(previous_fs);
    });


    $(".submit").click(function(){
        return false;
    });
    $(".submit-again").click(function(){
        sendData();
    })
});

function saveData(form){
    form.find("input[type!='button']").each(function(){
        localStorage.setItem(`input_${this.name}` , this.value)
    });
}
function mapData(form){
    form.find("input[type!='button']").each(function(){
        data = localStorage.getItem(`input_${this.name}`);
        this.value = data;
    });
}
function stepManager(current_fs , next_fs ){

    //Add Class Active
    $("#progressbar li").eq($("form").index(next_fs)).addClass("active");
    $("#progressbar li").eq($("form").index(next_fs)).prevAll().addClass("active");;
    //hide the current form with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            

            next_fs.css({'opacity': opacity});
        },
        duration: 600
    });

}
function sendData(){

    $(".final-image-ok").addClass('hidden');
    $(".final-image-cancel").addClass('hidden');
    $(".final-image-loader").removeClass('hidden');
    $(".submit-again").addClass('hidden');

    items = { ...localStorage };
    data = {};
    Object.keys(items).forEach(function(key) {

        if(key.includes("input"))
            data[key.replace("input_" , "")] = items[key];

    });
    formattedData = {
        tel: data.tel,
        firstName: data.first_name,
        lastName: data.last_name,
        address:[{
            houseNumber: data.house_no,
            city: data.city,
            street: data.street,
            zipCode: data.zip_code
        }],
        paymentInfo:{
            IBAN: data.IBAN,
            accountOwner: data.account_owner
        }
    };
    console.log(formattedData , JSON.stringify(formattedData));
    $.ajax({
        url: `${window.location.origin}/user`,
        type: "POST",
        data: formattedData ,
        dataType: "json",
        success: function (response) {

           // You will get response from your PHP page (what you echo or print)
           console.log(response);
           $(".final-image-loader").addClass('hidden');
           if(response.result == false){
                //try again
                
                $(".final-image-cancel").removeClass('hidden');
                $(".final .fs-title").html("Error");
                $(".final .fs-title").removeClass("hidden");
                $(".final .description").html("Error On  Signed Up <br> " + response.message);
                $(".final .description").removeClass("hidden");
                $(".final .submit-again").removeClass("hidden");
                return;
                
           }

                $(".final-image-ok").removeClass('hidden');
                $(".final .fs-title").html("Success !");
                $(".final .fs-title").removeClass("hidden");
                $(".final .description").html(`You Have Successfully Signed Up Your Code Is: ${response.paymentDataId}`);
                $(".final .description").removeClass("hidden");
           
        },
        error: function(jqXHR, textStatus, errorThrown) {
           //try again
           console.log(errorThrown);
           $(".final-image-loader").addClass('hidden');
            $(".final-image-cancel").removeClass('hidden');
            $(".final .fs-title").html("Error");
            $(".final .fs-title").removeClass("hidden");
            $(".final .description").html("Error On  Signed Up");
            $(".final .description").removeClass("hidden");
            $(".final .submit-again").removeClass("hidden");
        }
    });

}