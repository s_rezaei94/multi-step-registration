<?php

namespace App\AppDTO;

use App\AppInterface\AddressDTOInterface;

class AddressDTO  implements AddressDTOInterface{
    
    private $street;
    private $houseNumber;
    private $zipCode;
    private $city;
    
    public function __construct($data){
        
        $this->map($data);
    }
    //TODO validate
    public function map($data){
        
        foreach($data as $key => $value){
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

    }
  
    public function getStreet(){
        return $this->street;
    }
    public function getHouseNumber(){
        return $this->houseNumber;
    }
    public function getZipCode(){
        return $this->zipCode;
    }
    public function getCity(){
        return $this->city;
    }
    
}