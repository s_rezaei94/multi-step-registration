<?php

namespace App\AppClass;

abstract class HttpHandler{
    
    public $attempted;
    public $data;
    public abstract function getData();
    public abstract function checkData($data);
    public abstract function serviceUnAvailable();
    
    public function process(){
    
        $counter = 0;    
        do{
            $this->data = $this->getData();
            if($this->checkData($this->data))
                break;
            sleep(5);
            $counter++;
        }while($counter < $this->attempted);
        
        if(!$this->checkData($this->data)){
            $this->serviceUnAvailable();
        }
        return $this->data;
    }

}