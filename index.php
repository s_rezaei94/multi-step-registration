<?php

declare(strict_types = 1);

require_once "./vendor/autoload.php";


use App\AppClass\Router;

require_once "./app/Route/routes.php";
require_once "./app/Config/Constant.php";



(new Router($routes))->route();
















