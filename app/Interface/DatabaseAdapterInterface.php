<?php

namespace App\AppInterface;

/**
 * Interface of Database Adapter
 */
interface DatabaseAdapterInterface{

    public function insert(string $table , $params);
}