<?php

namespace App\Model;
use App\Util\Logger;

class PaymentInfoModel{
    use Logger;
    private $id;
    private $account_owner;
    private $IBAN;
    private $user_id;
    private $payment_data_id;
    private $tableName = "payment_info";

    public function __construct($user_id , $account_owner , $IBAN , $payment_data_id = null ){
        $this->setUserId($user_id);
        $this->setAccountOwner($account_owner);
        $this->setIBAN($IBAN);
        $this->setPaymentDataId($payment_data_id);
    }

    public function save($dbAdapter){
        $result = $dbAdapter->insert(
            $this->tableName,
            $this
        );
        if($result)
            return  $dbAdapter->insertId();
        
        $this->log("cant insert in PaymentInfoModel");
        $this->outputLog();
        
    }
    public function remove($dbAdapter){
        $result = $dbAdapter->delete(
            $this->tableName,
            $this
        );
        if($result)
            return true;
        
        $this->log("cant Remove in PaymentModel");
        $this->outputLog();

    }
    public function update($dbAdapter){
        $result = $dbAdapter->update(
            $this->tableName,
            ["payment_data_id" => $this->payment_data_id],
            ["user_id" => $this->user_id]
        );
        if($result)
            return true;
        
        $this->log("cant Update in PaymentModel");
        $this->outputLog();

    }
    public function setUserId($user_id){
        //TODO do some validation
        $this->user_id = $user_id;
    }
    public function setAccountOwner($account_owner){
        //TODO do some validation
        $this->account_owner = $account_owner;
    }
    public function setIBAN($IBAN){
        //TODO do some validation
        $this->IBAN = $IBAN;
    }
    public function setPaymentDataId($payment_data_id){
        //TODO do some validation
        $this->payment_data_id = $payment_data_id;
    }

    public function getProperties(){
        $properties = get_object_vars($this);
        //ommit the not usable field
        unset($properties['tableName']);
        return $properties;
    }
    
}