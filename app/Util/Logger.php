<?php

namespace App\Util;

trait Logger {

    
    public function log(string $message){

        $message  = date("Y-m-d h:i:s") . "  " . $message;
        file_put_contents(LOG_PATH . "log.log" , $message . PHP_EOL , FILE_APPEND);
    }
    public function outputLog(string $message = "Some thing went wrong!! Please see log for more details"){

        echo json_encode(["result" => false , "message" => $message]);
        die;
    }
    
}
