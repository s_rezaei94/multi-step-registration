<?php

//make sure all service up
sleep("5");


echo shell_exec("composer install");

$servername = getenv("MYSQL_HOST");
$username = getenv("MYSQL_USER");
$password = getenv("MYSQL_PASSWORD");
$dbName = "wunder";

$conn = null;
while(!$conn){
    try{

        $conn = new PDO("mysql:host=$servername;dbname=$dbName", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully";

    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
        echo "trying to connect to db" . "\n";
        sleep(5);
        $conn = null;
    }
}

try {
    
    $conn->exec("CREATE DATABASE IF NOT EXISTS `{$dbName}`;

    USE {$dbName};
    CREATE TABLE IF NOT EXISTS user(
        `id` int NOT NULL AUTO_INCREMENT,
        `first_name` varchar(255) NOT NULL,
        `last_name` varchar(255) NOT NULL,
        `tel` varchar(20) NOT NULL UNIQUE,
        PRIMARY KEY (`id`)

    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

    CREATE TABLE IF NOT EXISTS address(
        `id` int NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `street` varchar(255) NOT NULL,
        `house_number` varchar(20) NOT NULL,
        `zip_code` char(10) NOT NULL,
        `city` varchar(50) NOT NULL,
        PRIMARY KEY (`id`)

    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

    CREATE TABLE IF NOT EXISTS payment_info(
        `id` int NOT NULL AUTO_INCREMENT,
        `user_id` int(11) NOT NULL,
        `account_owner` varchar(255) NOT NULL,
        `IBAN` varchar(50) NOT NULL,
        `payment_data_id` varchar(255),
        PRIMARY KEY (`id`)

    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
    ");
    $conn = null;
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

echo "\n" . "*************** Done ***************";
echo "\n" . "*************** Done ***************";
