<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$routes = new RouteCollection();

$routes->add('createUser', 
(new Route(
    '/user',
    array('controller' => 'UserController', 'method'=>'create')
    
  ))->setMethods("POST")

);

$routes->add('test', 
(new Route(
    '/test',
    array('controller' => 'UserController', 'method'=>'test')
    
  ))->setMethods("POST")

);