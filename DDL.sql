

CREATE TABLE user(
    `id` int NOT NULL AUTO_INCREMENT,
    `first_name` varchar(255) NOT NULL,
    `last_name` varchar(255) NOT NULL,
    `tel` varchar(20) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE address(
    `id` int NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `street` varchar(255) NOT NULL,
    `house_number` varchar(20) NOT NULL,
    `zip_code` char(10) NOT NULL,
    `city` varchar(50) NOT NULL,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE payment_info(
    `id` int NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `account_owner` varchar(255) NOT NULL,
    `IBAN` varchar(50) NOT NULL,
    `payment_data_id` varchar(255),
    PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
