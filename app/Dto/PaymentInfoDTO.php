<?php

namespace App\AppDTO;

use App\AppInterface\PaymentInfoDTOInterface;

class PaymentInfoDTO  implements PaymentInfoDTOInterface{
    
    private $accountOwner;
    private $IBAN;
    
    public function __construct($data){
        
        $this->map($data);
    }
    //TODO validate
    public function map($data){
        foreach($data as $key => $value){
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }

    }
  
    public function getAccountOwner(){
        return $this->accountOwner;
    }
    public function getIBAN(){
        return $this->IBAN;
    }
    
}